<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Utils\Persistent;

class Brand extends Model
{
    use SoftDeletes, Persistent;

    public $timestamps = false;
    protected $fillable = [
        'name','image_url','status','promotion','keywords','seo_description','extra_html'
    ];

    protected $casts = [
        'status' => 'boolean',
        'promotion' => 'boolean',
    ];

    /**
     * 获取品牌的图片URL
     * @return bool|string
     */
    public function getImageUrl(){
        return $this->image_url ? asset('storage/'.$this->image_url) : false;
    }

    /**
     * 品牌产品的子系列
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function serials(){
        return $this->hasMany(BrandSerial::class);
    }

    /**
     * Get all promoted brands
     * @return Collection
     */
    public static function GetPromotionBrands(){
        return self::where('promotion',true)
            ->where('status',true)
            ->orderBy('name','asc')
            ->get();
    }
}
