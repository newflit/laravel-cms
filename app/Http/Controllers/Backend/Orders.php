<?php

namespace App\Http\Controllers\Backend;

use App\Models\Order\OrderItem;
use App\Models\Utils\JsonBuilder;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order\Order;
use App\Models\Utils\UserGroup;

class Orders extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 后台的订单管理
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function my_orders(Request $request)
    {
        $this->dataForView['menuName'] = 'orders';

        /**
         * @var User $user
         */
        $user = $request->user();

        if($user->isSuper()){
            // 超级管理员用户, 可以看所有的菜单
            $this->dataForView['orders'] = Order::orderBy('id','desc')->paginate(config('system.PAGE_SIZE'));
            $this->dataForView['vuejs_libs_required'] = ['my_orders'];
            return view(
                'backend.order.my_orders',
                $this->dataForView
            );
        }elseif ($user->isOperator()){
            $this->dataForView['orderItems'] = OrderItem::where('product_owner_id',$user->id)
                ->orderBy('id','desc')->paginate(config('system.PAGE_SIZE'));
            return view(
                'backend.order.my_order_items',
                $this->dataForView
            );
        }
    }

    /**
     * 后台查看订单详情
     * @param $orderId
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view($orderId, Request $request){
        $this->dataForView['menuName'] = 'dashboard';
        $this->dataForView['order'] = Order::find($orderId);

        $this->dataForView['vuejs_libs_required'] = ['view_order'];
        return view(
            'backend.order.view',
            $this->dataForView
        );
    }

    /**
     * 订单的ajax搜索
     * @param Request $request
     * @return string
     */
    public function ajax_search(Request $request){
        if($request->isMethod('post')){
            $keyword = $request->get('key');
            $orders = Order::select('id','place_order_number','status','total','delivery_charge')->where('place_order_number','like',$keyword.'%')
                ->orderBy('id','desc')
                ->take(config('system.PAGE_SIZE'))
                ->get();
            $result = [];
            foreach ($orders as $order) {
                $valueStr = $order->place_order_number.' - '.$order->group_name.', '.config('system.CURRENCY').$order->group;
                $result[] = ['value'=>$valueStr,'id'=>$order->id];
            }
            return JsonBuilder::Success($result);
        }
    }

    /**
     * 将订单切换为订单已发布状态
     * @param $id
     * @param Request $request
     * @return string
     */
    public function ajax_issue_invoice($id, Request $request){
        if(session('user_data') && session('user_data.role') == UserGroup::$ADMINISTRATOR){
            if(Order::IssueInvoice($id)){
                return JsonBuilder::Success();
            }else{
                return JsonBuilder::Error();
            }
        }
    }
}
