<?php
/**
 * 经销商相关操作的控制器类
 */
namespace App\Http\Controllers\Backend;

use App\Models\Group;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Groups extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 经销商后台管理 列表
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request){
        $this->dataForView['groups'] = Group::where('id','>',1)->orderBy('name','asc')->paginate(config('system.PAGE_SIZE'));
        $this->dataForView['menuName'] = 'groups';
        return view('backend.groups.index', $this->dataForView);
    }

    /**
     * 加载添加经销商表单视图
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(){
        $this->dataForView['group'] = new Group();
        $this->dataForView['menuName'] = 'groups';
        return view('backend.groups.form', $this->dataForView);
    }

    /**
     * 加载编辑经销商表单视图
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id){
        $this->dataForView['group'] = Group::find($id);
        $this->dataForView['menuName'] = 'groups';
        return view('backend.groups.form', $this->dataForView);
    }

    /**
     * 删除经销商的操作
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id){
        Group::where('id',$id)->delete();
        return redirect()->route('groups');
    }

    /**
     * 保存经销商信息的操作
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request){
        $groupData = $request->all();
        unset($groupData['_token']);

        if(empty($groupData['id'])){
            unset($groupData['id']);
            Group::Persistent($groupData);
        }else{
            $id = $groupData['id'];
            unset($groupData['id']);

            Group::where('id', $id)
                ->update($groupData);
        }

        return redirect()->route('groups');
    }
}
