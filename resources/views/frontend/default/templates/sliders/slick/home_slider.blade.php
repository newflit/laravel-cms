<?php
/**
 * 检查是否需要输出 Slick Carousel at Home page
 */
if(array_key_exists('slider_home_page',$sliders)){
    $homeSlider = $sliders['slider_home_page'];
}else{
    return;
}
$options = [
    'autoplay'      =>true,
    'autoplaySpeed' =>$homeSlider->interval,
    'slidesPerRow'  =>$homeSlider->images_per_frame,
    'fade'          =>true
];
?>
<div class="{{ $homeSlider->wrapper_classes }}">
    <div data-slick='<?php echo json_encode($options); ?>' class="slick-carousel-el" id="slick-carousel-el-slider_home_page" {{ $homeSlider->attributes_text }}>
        <?php $sliderImages = $homeSlider->getSliderImages(); ?>
        @foreach($sliderImages as $idx=>$sliderImage)
            @php
                /** @var \App\Models\Widget\SliderImage $wrapperTag */
                $wrapperTag = $sliderImage->html_tag ? $sliderImage->html_tag : 'div';
                $wrapperClass = $sliderImage->classes_name ? $sliderImage->classes_name : null;
                $linkTo = $sliderImage->link_to ? $sliderImage->link_to : '#';
                $animateClass = $sliderImage->animate_class ? 'animated '.$sliderImage->animate_class : null;
            @endphp
            <{{ $wrapperTag }} class="full-width slick-carousel-item {{ $wrapperClass }}" data-animate="{{ $animateClass }}">
                    <a href="{{ $linkTo }}">
                        <img class="full-width {{ $sliderImage->classes_name }}" data-lazy="{{ $sliderImage->media->url }}" alt="{{ $sliderImage->caption ? $sliderImage->caption : $homeSlider->name.$idx }}" />
                        {!! $sliderImage->extra_html !!}
                    </a>
            </{{ $wrapperTag }}>
        @endforeach
    </div>
</div>
