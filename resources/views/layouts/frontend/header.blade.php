<nav class="navbar custom-header container" role="navigation" aria-label="main navigation" id="navigation">
    <div class="navbar-brand">
        <a class="navbar-item logo-head-text" href="{{ url('/') }}">
            @if(empty($siteConfig->logo))
                {{ str_replace('_',' ',env('APP_NAME','Home')) }}
                @else
                {!! \App\Models\Utils\AMP\MediaUtil::NormalImage(asset($siteConfig->logo),'Logo', null, 80, 'logo-img') !!}
            @endif
        </a>
    </div>
    <div class="navbar-end">
        @foreach($rootMenus as $key=>$rootMenu)
            <?php
            $tag = $rootMenu->html_tag;
            $children = $rootMenu->getSubMenus();
            if($tag && $tag !== 'a'){
                echo '<'.$tag.'>';
            }
            ?>
            @if(count($children) == 0)
                <a class="{{ $rootMenu->css_classes }}" href="{{ url($rootMenu->link_to=='/' ? '/' : '/page'.$rootMenu->link_to) }}">
                    {{ app()->getLocale()=='cn' && !empty($rootMenu->name_cn) ? $rootMenu->name_cn : $rootMenu->name }}
                </a>
            @else
                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link" href="{{ $rootMenu->link_to == '#' ? '#' : url($rootMenu->link_to) }}">
                        {{ app()->getLocale()=='cn' && !empty($rootMenu->name_cn) ? $rootMenu->name_cn : $rootMenu->name }}
                    </a>
                    <div class="navbar-dropdown is-boxed">
                        @foreach($children as $sub)
                            <a class="navbar-item" href="{{ url('/page'.$sub->link_to) }}">
                                {{ app()->getLocale()=='cn' && !empty($sub->name_cn) ? $sub->name_cn : $sub->name }}
                            </a>
                        @endforeach
                    </div>
                </div>
            @endif
            <?php
            if($tag && $tag !== 'a'){
                echo '</'.$tag.'>';
            }
            ?>
        @endforeach
        <a class="navbar-item" href="{{ url('contact-us') }}">
            {{ trans('general.menu_contact') }}
        </a>
    </div>
</nav>
<div class="container-fluid full-width-menu-bar mb-20">
    <nav class="navbar container no-bg">
        <div id="navMenu" class="navbar-menu">
            <div class="navbar-start is-marginless">
                @if(isset($categoriesTree) && count($categoriesTree) > 0)
                    <div class="navbar-item pl-0">
                        <a id="product-category-root" class="button is-link has-text-left" href="#" style="width: 270px;height: 50px; background-color: {{ $siteConfig->theme_main_color?$siteConfig->theme_main_color:'#1953b4' }};">
                            {{ trans('general.All_Categories') }}&nbsp;<i class="fas fa-bars" style="position: absolute; right: 16px;top: 15px;"></i>
                        </a>
                    </div>
                @endif
            </div>
            <div class="navbar-end is-marginless">
                @if(env('activate_search_bar',false))
                    <div class="navbar-item">
                        <div id="navigation-app">
                            <el-autocomplete
                                class="must-on-layer-top advanced-auto-search input-with-select"
                                v-model="searchKeyword"
                                :fetch-suggestions="querySearchAsync"
                                placeholder="Search ..."
                                @select="handleSelect"
                                :trigger-on-focus="false"
                                style="border-left: none;"
                            >
                                <el-select v-model="selectedCategory" slot="prepend" placeholder="{{ trans('general.All_Categories') }}">
                                    <el-option label="餐厅名" value="1"></el-option>
                                    <el-option label="订单号" value="2"></el-option>
                                    <el-option label="用户电话" value="3"></el-option>
                                </el-select>
                                <el-button slot="append" icon="el-icon-search"></el-button>
                            </el-autocomplete>
                        </div>
                    </div>
                @endif
            </div>
            @if(env('activate_ecommerce',false))
                <div class="navbar-end">
                    <div class="navbar-item">
                        @include(_get_frontend_layout_path('frontend.shopping_cart'))
                    </div>
                </div>
            @endif
        </div>
    </nav>
</div>