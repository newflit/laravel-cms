<?php

namespace App;

use App\Models\Group;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\UserGroup;
use Stripe\Customer;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    const ERROR_CODE_EMAIL_UNIQUE       = 70;       // Email字段为unique
    const ERROR_CODE_EMAIL_REQUIRED     = 71;       // Email字段为必须
    const ERROR_CODE_CREATE_NEW_FAILED  = 72;       // 创建新用户记录失败

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role','phone','fax',
        'address','city','postcode','state','country','uuid','group_id','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * 根据给定的id, 查询uuid和id, 找到特定的用户
     * @param $uuid
     * @return mixed
     */
    public static function GetByUuidOrId($uuid){
        if(is_string($uuid) && strlen($uuid)>30){
            return self::GetByUuid($uuid);
        }else{
            return self::find($uuid);
        }
    }

    /**
     * 根据uuid获取用户
     * @param $uuid
     * @return mixed
     */
    public static function GetByUuid($uuid){
        return self::where('uuid',$uuid)->first();
    }

    /**
     * 根据给定的邮件获取用户
     * @param $email
     * @return mixed
     */
    public static function GetByEmail($email){
        return self::where('email',$email)->first();
    }

    /**
     * 获取所有的operator用户
     * @return mixed
     */
    public static function GetOperators(){
        return self::where('role',\App\Models\Utils\UserGroup::$OPERATOR)->get();
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function groups(){
        return $this->hasMany(UserGroup::class);
    }

    public function group(){
        return $this->belongsTo(Group::class);
    }

    public function addressText(){
        return $this->address.', '.$this->city.' '.$this->postcode.
        ', '.$this->state. ', '.$this->country;
    }

    /**
     * 获取客户关联的 stripe customer 信息
     * @return null|\Stripe\StripeObject
     */
    public function getStripeCustomer(){
        if(is_null($this->stripe_id)){
            return null;
        }else{
            return Customer::retrieve($this->stripe_id);
        }
    }

    /**
     * 创建一个和当前 user 相关联的 stripe customer 对象
     * @param $source
     * @return \Stripe\ApiResource
     */
    public function createStripeCustomer($source){
        $customer = Customer::create([
            'source'=>$source,
            'description'=>$this->name,
            'email'=>$this->email
        ]);
        if($customer){
            $this->stripe_id = $customer->id;
        }
        return $customer;
    }

    /**
     * 检查当前用户是否为管理员
     * @return bool
     */
    public function isSuper(){
        return $this->role === \App\Models\Utils\UserGroup::$ADMINISTRATOR;
    }

    /**
     * 检查当前用户是否为operator角色
     * @return bool
     */
    public function isOperator(){
        return $this->role === \App\Models\Utils\UserGroup::$OPERATOR;
    }
}
