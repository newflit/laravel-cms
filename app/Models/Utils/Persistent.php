<?php
/**
 * Created by PhpStorm.
 * User: justinwang
 * Date: 13/10/18
 * Time: 6:31 PM
 */

namespace App\Models\Utils;

trait Persistent
{
    /**
     * Execute a basic saving action for all kind of models. Use ID is the key if $key/$value are empty
     * @param $data
     * @param null $key
     * @param null $value
     * @return mixed
     */
    public static function Persistent($data, $key = null, $value = null){
        if(isset($data['id']) && !empty($data['id'])){
            // This is an update action
            $modelId = $data['id'];
            unset($data['id']);
            if(!empty($key) && !empty($value)){
                return self::where($key, $value)->update($data);
            }else{
                return self::where('id',$modelId)->update($data);
            }

        }else{
            // This is a create action
            return self::create($data);
        }
    }
}