<div class="top-bar">
    <div class="container">
        <div class="navbar-menu">
            <div class="navbar-end">
                <div class="navbar-item">
                    @if(!session('user_data.id'))
                        <div class="field is-grouped">
                            <p class="control">
                                <a class="bd-tw-button button" href="{{ url('frontend/customers/login') }}">
                                  <span class="icon"><i class="fas fa-sign-in-alt"></i></span>
                                  <span>{{ trans('general.Customer_Login') }}</span>
                                </a>
                            </p>
                            <p class="control">
                                <a class="bd-tw-button button" href="{{ url('frontend/customers/register') }}">
                                    <span class="icon"><i class="fab fa-wpforms"></i></span>
                                    <span>{{ trans('general.Register') }}</span>
                                </a>
                            </p>
                        </div>
                    @else
                        <div class="field is-grouped">
                            <p class="control">
                                <a class="bd-tw-button button no-border" href="{{ url('frontend/my_orders/'.session('user_data.uuid')) }}">
                                    <span class="icon"><i class="fab fa-wpforms"></i></span>
                                    <span>{{ trans('general.My_Orders') }}</span>
                                </a>
                            </p>
                            <p class="control">
                                <a class="bd-tw-button button no-border" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                    <span class="icon"><i class="fas fa-sign-out-alt"></i></span>
                                    <span>{{ trans('general.Logout') }}</span>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="hidden">
                                        @csrf
                                    </form>
                                </a>
                            </p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>