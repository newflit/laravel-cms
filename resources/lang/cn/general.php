<?php
return [
    'menu_home' => '首页',
    'menu_contact' => '联系我们',
    'seo_description_note' => 'Keywords 和 Description不需要使用回车字符',
    'system_busy'=>'系统繁忙, 请稍候再试!',
    'Required'=>'必填',
    'Optional'=>'选填',
    'menu_payment_methods'=>'支付方式管理',
    'menu_shipment'=>'运费管理',
    'menu_wechat'=>'微信公众号',
    'menu_agent'=>'合作经销商管理',
    // 用户需要的通用字段
    'Customer_Login'=>'登陆',
    'Register'=>'注册',
    'My_Orders'=>'我的订单',
    'Logout'=>'退出',
    'All_Categories'=>'产品目录',
    'Price'=>'价格',
];